mod_adj_UI <- function(id) {
  ns <- NS(id)

  tagList(

    # Topline panel with no contents, placeholder for CSS spinner
    column(8, 
      withSpinner(htmlOutput(ns("topline")), color = "#29428D"),
    ),

    fluidRow(box(
      width =  '8',
      tableOutput(ns("table_adjustments"))
    ))
  )
}


mod_adj <- function(input, output, session, CCAODATA, get_diag_lst) {

  # Placeholder UI element to be wrapped by CSS spinner
  output$topline <- renderText({
    validate(check_diag_lst(get_diag_lst()))
    paste0(
      h3("2020 COVID-19 Adjustments"),
    
      h5(HTML(paste(
        tags$b("NOTE:"),
        "Do not communicate adjusted values to taxpayers if the Preliminary Value Source is 2020 Modeling Pipeline.
        These values have not yet been desk reviewed and are still subject to change."
      )))
    )
  })
  
  adjustments <- readRDS("data/adjustments.rds")
  
  output$table_adjustments <- renderTable({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    
    pin10 <- str_sub(get_diag_lst()$diag_pin_value, 1, 10)
    
    validate(
      need(pin10 %in% adjustments$pin, "PIN missing in adjustments data")
    )
    
    adj_filtered <- adjustments %>%
      filter(pin == pin10)
    
    validate(
      need(nrow(adj_filtered) == 1, "Duplicate or missing rows of adjustment data")
    )
    
    tribble(
      ~x1, ~x2, 
      
      "2020 Preliminary Value", ifelse(
        is.na(adj_filtered$per_ass),
        dollar(adj_filtered$ref_val, 1),
        dollar(adj_filtered$per_ass * adj_filtered$ref_val, 1)
      ),
      "Preliminary Value Source", adj_filtered$ref,
      "COVID Adjustment Group", adj_filtered$group,
      "Avg. NBHD COVID Adjustment", percent(adj_filtered$avg_nbhd_adj / 100),
      "COVID Adjusted Value", ifelse(
        is.na(adj_filtered$per_ass),
        dollar(adj_filtered$adj_val, 1),
        dollar(adj_filtered$per_ass * adj_filtered$adj_val, 1)
      )
    )
    
  },
  colnames = FALSE,
  striped = TRUE,
  na = " - ",
  align = "lc",
  width = "100%"
  )

}
