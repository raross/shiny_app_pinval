mod_unif_UI <- function(id) {
  ns <- NS(id)

  tagList(

    # Topline panel that display stats about number of uniformity comps found,
    # the median $/sqft of those comps, and other topline takeaways
    fluidRow(
      column(8,
        withSpinner(htmlOutput(ns("topline")), color = "#29428D")
      )
    ),

    # Panel that displays only if uniformity comps are found
    conditionalPanel(
      ns = ns,
      condition = "output.unif_boolean",

      fluidRow(box(
        width = '8',
        title = tags$p(tags$b("Table 1:"), "Uniformity Parameters",  definition("comps_uniform", placement="right")),
        # Summary table of the boundaries used to find uniformity comps
        tableOutput(ns("table_uniformity_bounds")),
      )),
      fluidRow(box(
        width = '8',
        title = tags$p(tags$b("Figure 1:"), "Uniformity Comparables Map"),
        # Leaflet map displaying the location of each comps
        withSpinner(leafletOutput(ns("plot_uniformity_map")), color = "#29428D"),
      )),
      fluidRow(box(
        width = '8',
        title = tags$p(tags$b("Figure 2:"), "Uniformity Comparables Characteristics"),
        # Plotly interactive histogram displaying the distribution of comp's
        # characteristic data
        plotlyOutput(ns("plot_uniformity_comps"), height = "650px"),
      )),
      fluidRow(box(
        width = '8',
        title = tags$p(tags$b("Table 2:"), "Uniformity Comparables Table"),
        # Interactive table displaying uniformity comps. Target PIN is highlighted
        # in red. Characteristics matching the subject PIN are highlighted in
        # green
        dataTableOutput(ns("table_uniformity_comps_ref"))         
      ))
    )
  )
}


mod_unif <- function(input, output, session, CCAODATA, get_diag_lst, get_unif_df) {
  cdata <- session$clientData

  # Raw HTML out that includes the topline conclusions for this tab
  output$topline <- renderText({
    validate(check_diag_lst(get_diag_lst()))
    validate(check_unif_df(get_unif_df()))

    status <- unif_status(get_diag_lst, get_unif_df)

    # HTML out that includes the above variables
    paste0(
      h2("Uniformity Comparables"),

      p(
        "Uniformity comparables are properties that fall within a standard, fixed range of this property's characteristics. 
        They are often used to assess whether a property has been assessed fairly, or uniformly.",
        class="lead"
      ),

      wellPanel(class="comps-description",
        div(class = "legend",
          div(class = "legend-scale",
            tags$ul(class = "legend-labels",
              tags$li(HTML(paste(
                tags$span(style = paste(
                    "background:", 
                    ifelse(
                      abs(status$unif_pct_dev_med) <= 0.05, 
                      "#81ca9c", 
                      "#ffdf99"
                    ), ";"
                )),
                "This property's price per building square foot is",
                tags$b(percent(abs(status$unif_pct_dev_med), 0.01)),
                tags$b(ifelse(sign(status$unif_pct_dev_med) == 1, "above", "below")),
                sprintf("the median, %s the 5%% threshold.", ifelse(
                  abs(status$unif_pct_dev_med) <= 0.05,
                  "<b>within</b>",
                  "<b>not within</b>"
                ))
              ))),
              tags$li(HTML(paste(
                tags$span(style = paste(
                    "background:", 
                    ifelse(
                      status$unif_number_comps >= 6,
                      "#81ca9c", 
                      "#ffdf99"
                    ), ";"
                )),
                "Found", tags$b(status$unif_number_comps), 
                "comparables from", get_diag_lst()$diag_mryca_year,
                "using the parameters below, which",
                ifelse(
                  status$unif_number_comps >= 6,
                  "<b>meets</b>",
                  "<b>does not meet</b>"
                ),
                "the minimum requirement of 6 comparables."
              )))
            )
          )
        )
      )
    )
  })

  # Create an output boolean for whether or not this PIN has comps, this boolean
  # is used by the conditionalPanel() function to determine whether or not to
  # display the interactive part of the uniformity output
  output$unif_boolean <- reactive({
    is.null(check_unif_df(get_unif_df()))
  })
  outputOptions(output, "unif_boolean", suspendWhenHidden = FALSE)

  # Simple table that displays the boundaries used to find uniformity comps
  # These are always the target PIN's characteristic * some scaling factor
  output$table_uniformity_bounds <- renderTable(
    {
      validate_no_msg(check_diag_lst(get_diag_lst()))
      validate_no_msg(check_unif_df(get_unif_df()))
      table_unif_bounds(get_diag_lst(), get_unif_df())
    },
    colnames = TRUE,
    striped = FALSE,
    na = "",
    align = "lccc",
    width = "75%",
    sanitize.text.function = function(x) x
  )

  # Interactive leaflet map displaying the location of comps and the
  # neighborhood they're within
  output$plot_uniformity_map <- renderLeaflet({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(check_unif_df(get_unif_df()))
    plot_unif_map(get_diag_lst(), get_unif_df())
  })

  # Interactive reference table showing each uniformity comp and its
  # characteristics. 
  output$table_uniformity_comps_ref <- renderDataTable(
    {
      validate_no_msg(check_diag_lst(get_diag_lst()))
      validate_no_msg(check_unif_df(get_unif_df()))
      table_unif_comps_ref(get_diag_lst(), get_unif_df())
    },
    server = FALSE
  )

  # Plotly plot that displays each PIN in a discrete histogram
  output$plot_uniformity_comps <- renderPlotly({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(check_unif_df(get_unif_df()))
    plot_unif_comps(
      get_diag_lst(), get_unif_df(),
      cdata$output_pid_width,
      cdata$output_pid_height
    )
  })
}

unif_status <- function(get_diag_lst, get_unif_df) {
  validate(check_diag_lst(get_diag_lst()))
  validate(check_unif_df(get_unif_df()))

  # Median price per square foot of all uniformity comps
  unif_median_price_sqft <- get_unif_df() %>%
    pull(DOLLAR_SQFT) %>%
    median(., na.rm = TRUE)

  # Price per square foot of only our target PIN
  unif_target_price_sqft <- get_unif_df() %>%
    filter(PIN == get_diag_lst()$diag_pin_value) %>%
    pull(DOLLAR_SQFT)

  # Percent deviation from the median
  unif_pct_dev_med <- (
    (unif_target_price_sqft - unif_median_price_sqft) / unif_median_price_sqft
  )

  unif_number_comps <- get_unif_df() %>%
    nrow() - 1

  return(list("unif_pct_dev_med"=unif_pct_dev_med, "unif_number_comps"=unif_number_comps))
}
