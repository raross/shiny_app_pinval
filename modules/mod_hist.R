mod_hist_UI <- function(id) {
  ns <- NS(id)

  tagList(

    # Topline panel with no contents, placeholder for CSS spinner
    fluidRow(
      column(8,
        withSpinner(htmlOutput(ns("topline")), color = "#29428D"),
      )
    ),
    
    conditionalPanel(
      ns=ns,
      condition = "output.assmnt_boolean",
      fluidRow(column(8,	
        bsCollapse(open="reference",
          bsCollapsePanel(
            value="reference",
            "Reference Data",
            tags$ul(
              tags$li(htmlOutput(ns("text_pin_sales"))),
              tags$li(htmlOutput(ns("assement_yrs"))),
              tags$li(
                "Fair Market Value", 
                definition("fmv", placement="bottom"),
                "calculation does not necessarily take into account the reduction from a previous appeal.")
            ),
            style="info"
          )
        )	
      )),
    ),
    
    # History plot that displays appeals, sales, etc, over time
    conditionalPanel(
      ns = ns,
      condition = "output.assmnt_boolean",
      fluidRow(box(
        width = '8',
        title =  tags$p(tags$b("Figure 1:"), "History of This Property's Assesments and Appeals"),
        plotOutput(ns("plot_history_fmv"), height = "650px")
      ))
    ),
    
    # Appeals table that appears only if there are appeals for this PIN
    conditionalPanel(
      ns = ns,
      condition = "output.appeals_boolean",
      fluidRow(box(
        width = '8',
        title =  tags$p(tags$b("Table 1:"), "This Property's Appeal History and Comments"),
        dataTableOutput(ns("table_history_fmv_ref"))
      ))
    ),
    
    # Conditional panel that appears if a PIN is a 288
    conditionalPanel(
      ns = ns,
      condition = "output.h288s_boolean",
      fluidRow(box(
        width = '8',
        title =  tags$p(tags$b("Table 2:"), "288 Characteristics"),
        p(
          tags$span("Yellow", class = "important-text-yellow"),
          "indicates a changed characteristic for a given year."
        ),
        dataTableOutput(ns("table_h288s_changes"))
      ))
    )
  )
}


mod_hist <- function(input, output, session, CCAODATA, get_diag_lst,
                     get_appeals_df, get_assmnt_df, get_h288s_df) {

  # Placeholder UI element to be wrapped by CSS spinner
  output$topline <- renderText({
    validate(check_diag_lst(get_diag_lst()))

    paste(
      p(
        "View this propety's history of assessments and appeals.",
        class="lead"
      )
    )
  })
  
  output$text_pin_sales <- renderText({
    req(is.null(check_diag_lst(get_diag_lst())))
    req(!is.na(get_diag_lst()$diag_sale_date[1]))
    paste(
      "The most recent sale on was ", get_diag_lst()$diag_sale_date[1],
      "for", dollar(get_diag_lst()$diag_sale_price[1]), "."
    )
  })
  
  
  output$assement_yrs <- renderText({
    req(is.null(check_diag_lst(get_diag_lst())))
    
    next_ry <- get_diag_lst()$diag_next_reass_year
    last_ry <- ccao::town_get_assmnt_year(
      get_diag_lst()$diag_town_num,
      round_type = "floor"
    )
    
    paste0(
      "This property was last reassessed in ", last_ry,
      ". The next reassessment year is ",
      ifelse(last_ry == next_ry, next_ry + 3, next_ry), "."
    )
  })
  
  # Create an output boolean for whether or not this PIN has the data needed
  # to display the appeals table
  output$appeals_boolean <- reactive({
    is.null(check_appeals_df(get_appeals_df()))
  })
  outputOptions(output, "appeals_boolean", suspendWhenHidden = FALSE)
  
  # Create an output boolean checking whether the data to display the history
  # plot is available
  output$assmnt_boolean <- reactive({
    is.null(check_assmnt_df(get_assmnt_df()))
  })
  outputOptions(output, "assmnt_boolean", suspendWhenHidden = FALSE)

  # Plot displaying target PIN's FMV over time
  output$plot_history_fmv <- renderPlot({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate(check_assmnt_df(get_assmnt_df()))
    plot_hist_fmv(get_diag_lst(), get_assmnt_df(), get_appeals_df(), get_h288s_df())
  })

  # Table showing CCAO appeals (no Board) for the selected timeframe
  output$table_history_fmv_ref <- renderDataTable({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate(check_appeals_df(get_appeals_df()))
    table_hist_fmv_ref(get_diag_lst(), get_appeals_df())
  })

}
