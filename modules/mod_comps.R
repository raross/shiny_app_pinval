mod_comps_UI <- function(id) {
  ns <- NS(id)

  wellPanel(class="comps-description",
    p("How does this property compare?"),
    div(class = "legend",
      div(class = "legend-scale",
        tags$ul(class = "legend-labels",
          tags$li(htmlOutput(ns("comps_unif_summ"))),
          tags$li(htmlOutput(ns("comps_nbhd_summ"))),
          tags$li(htmlOutput(ns("comps_sales_summ")))
        )
      )
    )
  )
}


mod_comps <- function(input, output, session, CCAODATA, get_diag_lst, get_unif_df, get_assmnt_df, get_comps_df) {
  
  # call these other modules to get their variables
  # TODO - is this expensive?
  callModule(mod_unif, "unif", CCAODATA, get_diag_lst, get_unif_df)

  ## Summaries for Comparables Sub-Tabs ##
  # TODO - this is repeated code - in fact, this modularization is pretty bad
  # try using reactive vars like so https://shiny.rstudio.com/articles/communicate-bet-modules.html

  output$comps_unif_summ <- renderText({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(check_unif_df(get_unif_df()))

    status <- unif_status(get_diag_lst, get_unif_df)
    paste(
      tags$span(style = paste(
          "background:", 
          ifelse(
            # within the 5% threshold and sufficient comparables
            abs(status$unif_pct_dev_med) <= 0.05 && status$unif_number_comps >= 6, 
            # green
            "#81ca9c", 
            ifelse(
              # not sufficient, then yellow - uncertain status
              status$unif_number_comps < 6,
              # yellow
              "#ffdf99",
              # sufficient and not within the margin - bad status
              #red
              "#F8757599"
            )
          ), ";"
      )),
      "Based on",
      ifelse(
        status$unif_number_comps >= 6,
        paste(
          "a <span class='normal important-text-green'>sufficient</span> number of <b>Uniformity</b>",
          definition("comps_uniform", placement="below"),
          "comparables."
        ),
        paste(
          "an <span class='normal important-text-yellow'>insufficient</span> number of <b>Uniformity</b>",
          definition("comps_uniform", placement="below"),
          "comparables."
        )
      ),
      "this property is",
      ifelse(
        # within the threshold
        abs(status$unif_pct_dev_med) <= 0.05,
        "<span class='normal important-text-green'>within</span> the acceptable deviation threshold.",
        "<span class='normal important-text-red'>not within</span> the acceptable deviation threshold."
      )
    )
  })

  output$comps_nbhd_summ <- renderText({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(check_assmnt_df(get_assmnt_df()))

    diag_lst <- get_diag_lst()
    assmnt_df <- get_assmnt_df()

    percentile <- assmnt_df %>%
      filter(!is.na(bor_av_else_latest)) %>%
      filter(TAX_YEAR == diag_lst$diag_mryad_year) %>%
      filter(CLASS %in% diag_lst$diag_class) %>%
      mutate(percentile = percent_rank(bor_av_else_latest), median=median(bor_av_else_latest)) %>%
      filter(PIN == diag_lst$diag_pin_value)

    percent_diff <- (diag_lst$diag_mryad_value-percentile$median)/percentile$median

    paste(
      tags$span(style = paste(
          "background: ",
          color_palette(100-2*abs(50-100*percentile$percentile), c("#F8757599", "#ffdf99", "#81ca9c"))
      )),
      "This property is valued higher than", 
      percent(percentile$percentile), 
      "of properties in the same class and <b>Neighborhood</b>,",
      percent(percent_diff),
      ifelse(sign(percent_diff)==1, "above", "below"),
      "the median."
    )
  })

  output$comps_sales_summ <- renderText({
    comps_df <- get_comps_df()
    diag_lst <- get_diag_lst()

    validate_no_msg(check_diag_lst(diag_lst))
    validate_no_msg(check_comps_df(comps_df))

    percentile <- comps_df %>%
      filter(COMP_CLASS %in% diag_lst$diag_class) %>%
      # add a row for this pin - instead of the sale price, substitute the FMV
      add_row(COMP_PIN = diag_lst$diag_pin_value, SALE_PRICE = diag_lst$diag_mryad_value) %>%
      # compare the FMV to the rest of the distribution
      mutate(percentile = percent_rank(SALE_PRICE), median=median(SALE_PRICE)) %>%
      filter(COMP_PIN == diag_lst$diag_pin_value)

    percent_diff <- (diag_lst$diag_mryad_value-percentile$median)/percentile$median

    paste(
      tags$span(style = paste(
          "background: ",
          # transform the percentile to represent the distance from the median
          color_palette(100-2*abs(50-100*percentile$percentile), c("#F8757599", "#ffdf99", "#81ca9c"))
      )),
      "This property is valued higher than", 
      percent(percentile$percentile), 
      "of <b>Sales</b>",
      definition("comps_sales", placement="bottom"),
      "comparables in the same class,",
      percent(percent_diff),
      ifelse(sign(percent_diff)==1, "above", "below"),
      "the median."
    )
  })
}


