mod_nbhd_UI <- function(id) {
  ns <- NS(id)
  
  tagList(

    column(8,
      withSpinner(htmlOutput(ns("topline")), color = "#29428D")
    ),

    # Conditional panel that only appears when assessment data is valid
    conditionalPanel(
      ns = ns,
      condition = "output.assmnt_boolean",

      column(8,
        uiOutput(ns("input_class_filter"))
      ),
      
      fluidRow(box(
        width = '8',
        title = tags$p(tags$b("Figure 1:"), "Fair Market Value of This Property Compared to Its Neighborhood"),
        # Histogram of the distribution of FMVs for ALL PINs in neighborhood
        plotOutput(ns("plot_uniformity_dist"), height = "500px"),
      )),
      fluidRow(box(
        width = '8',
        title = tags$p(tags$b("Figure 2:"), "Fair Market Value of This Property Over Time"),
        # Plot showing target PIN FMV over time
        plotOutput(ns("plot_uniformity_history"), height = "500px")
      ))
    )
  )
}


mod_nbhd <- function(input, output, session, CCAODATA, get_diag_lst, get_assmnt_df) {
  ns <- session$ns

  # Input for selecting what distributions to show: same class only, all classes, or both.
  output$input_class_filter <- renderUI({
    selectInput(
      inputId=ns("class_filter"), 
      label="Viewing", 
      choices=setNames(
        as.list(c("same", "all", "both")), 
        c(
          "Properties in the same class as this property",
          "Properties in any class",
          "Show me the difference"
        )
      ),
      selected="same",
      width="45%"
    )
  })

  output$assmnt_boolean <- reactive({
    is.null(check_assmnt_df(get_assmnt_df()))
  })
  outputOptions(output, "assmnt_boolean", suspendWhenHidden = FALSE)
  
  # Raw HTML out that includes the topline conclusions for this tab
  output$topline <- renderText({

    diag_lst <- get_diag_lst()

    validate(check_diag_lst(diag_lst))
    validate(check_assmnt_df(get_assmnt_df()))

    nbhd_class <- get_assmnt_df() %>%
      filter(CLASS == diag_lst$diag_class) %>%
      filter(!is.na(bor_av_else_latest)) %>%
      filter(TAX_YEAR == diag_lst$diag_mryad_year)

    nbhd_expanded <- get_assmnt_df() %>%
      filter(CLASS != diag_lst$diag_class) %>%
      filter(!is.na(bor_av_else_latest)) %>%
      filter(TAX_YEAR == diag_lst$diag_mryad_year)

    paste0(
      h2("Neighborhood Comparables"),
      p("All residential properties in this property's neighborhood.", class="lead"),
      wellPanel(class="comps-description",
        tags$ul(
          tags$li(
            HTML(paste0(
              tags$b(nrow(nbhd_class)), 
              " neighborhood comparables in the ", tags$b("same class"), " as this property ",
              "(", get_diag_lst()$diag_mryad_year, ")"
            ))
          ),
          tags$li(
            HTML(paste0(
              tags$b(nrow(nbhd_expanded)), 
              " neighborhood comparables in a ", tags$b("different class "),
              "(", get_diag_lst()$diag_mryad_year, ")"
            ))
          )
        )
      )
    )
  })

  # Histogram of the distribution of FMVs for ALL PINs in neighborhood
  output$plot_uniformity_dist <- renderPlot({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(check_assmnt_df(get_assmnt_df()))
    req(input$class_filter)
    plot_nbhd_dist(get_diag_lst(), get_assmnt_df(), input$class_filter)
  })
  
  # History of the distribution of FMVs for ALL PINs in neighborhood
  output$plot_uniformity_history <- renderPlot({
    validate_no_msg(check_diag_lst(get_diag_lst()))
    validate_no_msg(check_assmnt_df(get_assmnt_df()))
    req(input$class_filter)
    plot_nbhd_history(get_diag_lst(), get_assmnt_df(), input$class_filter)
  })
}