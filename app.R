# Load libraries and environmental variables from a separate file
source("R/setup.R", local = TRUE)

# Getting MSSQL credentials from the shinyproxy server. This happens on app
# creation and the connection is closed when the app stops
CCAODATA <- dbConnect(
  odbc::odbc(),
  .connection_string = Sys.getenv("DB_CONFIG_CCAORSRV")
)

##### MAIN APPLICATION #####

ui <- dashboardPage(
  skin = "black",
  
  dashboardHeader(
    title = "Cook County Assessor's Office",
    titleWidth = 420
  ),

  dashboardSidebar(
    width = 420,

      # Title of the entire application, as shown on the sidebar
      column(12,
        titlePanel(
          h3("PIN Valuation (PINVAL) Report"),
          windowTitle="PINVAL - CCAO PIN Valuation Report"
        )
      ),

      # Version number and bug reporting button
      column(12,
        mod_gitlab_UI("gitlab"),
        hr(class = "sidebar-hr")
      ),
    
      # Row that contains entry box for PIN and the submit button, uses
      # CSS from style.css for proper sizing
      column(12,
        mod_input_UI("pin_input")
      ),

      column(12,
        mod_print_UI("print"),
        hr(class = "sidebar-hr")
      ),
      
      # Create a legend for status
      column(12,
        div(class = "legend",
          div(class = "legend-scale",
            div(class = "legend-sidebar",
              tags$ul(class = "legend-labels",
                tags$li(h4("Status Colors")),
                tags$li(tags$span(style = "background:#81ca9c;"), tags$p("All OK")),
                tags$li(tags$span(style = "background:#ffdf99;"), tags$p("Use Caution"))
              )            
            )
          )
        )   
      )    
  ),

  # Create a Shiny UI object that contains the page layout for the application
  # Define the main panel containing the individual tabs filled with content
  dashboardBody(

    # Load style.css from the www/ folder, this is loaded on shiny launch and
    # R may need to be reloaded to reflect changes in this file
    tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "style.css")),
    tags$head(tags$style(HTML(' .main-header .sidebar-toggle:before { content: "\\f104"; }'))),
    tags$head(tags$style(HTML(' .sidebar-collapse .main-header .sidebar-toggle:before { content: "\\f105";}'))),


    # Add javascript libraries/helper to the head of our HTML file
    # This allows us to use arbitrary javascript functions and shinyFeedback    
    useShinyFeedback(),
    useShinyjs(),
    tags$head(tags$script(src = "jquery.js")),
    tags$head(htmltools::htmlDependency("shinyBS", packageVersion("shinyBS"), src=c("href"="sbs"), script="shinyBS.js", stylesheet="shinyBS.css")),

    # Tabset panel for the main application, each tab loads its own module
    tabsetPanel(
      type = "tabs", id = "tabs",
      tabPanel("Summary", value = "summ_tab", mod_summ_UI("summ")),
      tabPanel("History and Appeals", value = "hist_tab", 
        tagList(
          column(8,
            h1("History and Appeals"),            
            tags$ul(
              tags$li("This Tab displays the History of the Fair Market Value Assesments and Appeals by the Cook County Assesor's Office and The Board of Review over time."),
              tags$li("Fair Market Value calculation does not nescessarily take into account the reduction from a previous appeal"),
            )
          ),
          mod_hist_UI("hist")
        )
      ),
      tabPanel("Comparables", value = "comps_tab",
        h1("Comparables"),
        mod_legend_UI("legend"),
        fluidRow(column(8,
          p(
            "Compare this property's valuations to the valuations of similar properties.",
            class="lead"
          ),
          mod_comps_UI("comps"),
          bsCollapse(id = "collapseExample",
            bsCollapsePanel(
              "What are comparables?",
              fluidRow(column(12,
                tags$ul(class="comps-description",
                  tags$li("Two properties are comparable to each other if they have similar characteristics. Similarity can be defined several ways."),
                  tags$li(HTML(paste0(
                    "While ",
                    strong("the assessment model does not use comparables to make valuations"),
                    ", comparable properties should have similar assessed values."
                  )))
                ),
                p(class="comps-description",
                  "Click the buttons below to explore the different types of comparable properties."
                )
              )), style="info"
            )
          ),        
        )),
        tabsetPanel(
          type = "pills", id = "tabs_comps",
          tabPanel("Uniformity", value = "unif_tab", 
            fluidPage(
              mod_unif_UI("unif")
            ),
            mod_download_UI("download_unif")
          ),
          tabPanel("Neighborhood", value = "nbhd_tab", 
            fluidPage(
              mod_nbhd_UI("nbhd")
            )
          ),
          tabPanel("Sales", value = "comps_tab",
            fluidPage(
              mod_sales_UI("comps")
            ), 
            mod_download_UI("download_comps")
          )
        )
      ),
      tabPanel("Adjustments", value = "adj_tab", mod_adj_UI("adj")),
      # Secondary tabset panel that appears ONLY when someone clicks the
      # View Full Report button. This tab contains ALL content on a single
      # page but is hidden in the tab select menu
      tabPanel(
        "All",
        value = "print_tab",
        fluidRow(
          mod_summ_UI("print_summ"),
          mod_hist_UI("print_hist"),
          mod_nbhd_UI("print_nbhd"),
          mod_unif_UI("print_unif"),
          mod_sales_UI("print_comps"),
          mod_adj_UI("print_adj")
        )
      )
    )
  )
)


# Define the server function that handles the backend logic of the application
server <- function(input, output, session) {

  # This module handles the sidebar inputs and validation functionality
  # It takes the service desk URL as an input so that it can generate a bug
  # reporting button
  # It outputs a list of reactives that contains:
  # 1. The clean PIN, 2. A reactive for when PIN is submitted
  pin_input <- callModule(mod_input, "pin_input")
  
  # Create the GitLab version number and bug reporting button on the sidebar
  callModule(mod_gitlab, "gitlab", pin_input$pin_value)
  
  # Module that handles collecting entered PIN information and reporting it
  # to a separate monitoring server
  callModule(mod_monitor, "monitor", pin_input$pin_submit, pin_input$pin_value)
  
  # Create a legend in the sidebar using info from our ingested data frames
  callModule(
    mod_legend, "legend", get_diag_lst
  )

  # Source event reactives for getting data objects from the DB
  # These reactives server data to different plotting and table functions, they
  # all update when the Submit PIN button is pressed. They exist only
  # within the server() function
  source("R/reactives.R", local = TRUE)
  
  # This block of modules contains both the server-side behavior and the UI
  # definitions for each tab. See the modules/ folder for individual tab/UI
  # element behavior and functions
  callModule(mod_summ, "summ", CCAODATA, get_diag_lst,get_condos_sales_df, get_appeals_df, get_assmnt_df, get_h288s_df)
  callModule(mod_hist, "hist", CCAODATA, get_diag_lst, get_appeals_df, get_assmnt_df, get_h288s_df)
  callModule(mod_nbhd, "nbhd", CCAODATA, get_diag_lst, get_assmnt_df)
  callModule(mod_unif, "unif", CCAODATA, get_diag_lst, get_unif_df)
  callModule(mod_sales, "sales", CCAODATA, get_diag_lst, get_comps_df)
  callModule(mod_adj, "adj", CCAODATA, get_diag_lst)
  callModule(mod_comps, "comps", CCAODATA, get_diag_lst, get_unif_df, get_assmnt_df, get_comps_df)

  # This block of modules creates the download buttons in the sidebar
  # It allows you to directly download the reactive dataframe specified
  callModule(
    mod_download, "download_unif", get_diag_lst, get_unif_df, 
    check_unif_df, "unif_comps.csv", "Download Uniformity Comparables (.csv)"
  )
  callModule(
    mod_download, "download_comps", get_diag_lst, get_comps_df,
    check_comps_df, "sales_comps.csv", "Download Sales Comparables (.csv)"
  )

  # This block of modules is called when the print button is pressed
  # It renders the entire report on a single hidden tab and updates the
  # View Full Report button to display Print Report when called
  callModule(mod_print, "print", get_diag_lst,
    parent_session = session,
    reactive({ input$tabs == "print_tab" }),
    pin_input$pin_submit
  )
  callModule(mod_summ, "print_summ", CCAODATA, get_diag_lst,get_condos_sales_df, get_appeals_df, get_assmnt_df, get_h288s_df)
  callModule(mod_hist, "print_hist", CCAODATA, get_diag_lst, get_appeals_df, get_assmnt_df, get_h288s_df)
  callModule(mod_nbhd, "print_nbhd", CCAODATA, get_diag_lst, get_assmnt_df)
  callModule(mod_unif, "print_unif", CCAODATA, get_diag_lst, get_unif_df)
  callModule(mod_sales, "print_comps", CCAODATA, get_diag_lst, get_comps_df)
  callModule(mod_adj, "print_adj", CCAODATA, get_diag_lst)
}


##### RUNNING THE APP #####

# Define what the shiny app does on start and on shutdown. Each app instance
# creates a database connection on start and closes it on shutdown
onStart <- function() {
  cat("Doing application setup\n")

  # Release database connections on app shutdown, if they exist
  onStop(function() {
    cat("Doing application cleanup\n")
    if (exists("CCAODATA")) dbDisconnect(CCAODATA)     
    if (exists("SHINYMONITOR")) dbDisconnect(SHINYMONITOR) 
  })
}


# Shiny app definition that gets called when app.R is run
# This is the line that actually launches the app
shinyApp(ui, server, onStart)
