# CCAO PIN Valuation Application (PINVAL)

### [Click here to access the production version of PINVAL](https://datascience.cookcountyassessor.com/shiny/app/pinval) (must be on CCAO internal network)

PINVAL is a Shiny web application that displays information about an individual Property Index Number (PIN). PINVAL's primary goal is to enable better, faster, more transparent, more consistent decision-making on residential valuation appeals. It's secondary goal is to be a central "source of truth" about the state and history of an individual property.

## Getting Set Up

PINVAL is built using R Shiny and can be run locally for testing and development.

[R](https://cloud.r-project.org/) and [RStudio](https://rstudio.com/products/rstudio/download/) must be installed to develop PINVAL locally. PINVAL uses R version 3.6.1 by default, but any version of R after 3.6.1 should work.

### renv

PINVAL depends on a number of third-party R libraries. It uses the [renv](https://rstudio.github.io/renv/articles/renv.html) package to manage these dependencies. renv creates a project-specific library of R packages using the packages and versions specified in [renv.lock](./renv.lock).

To prepare your development environment using `renv`, complete the following steps:

1. Clone the PINVAL repository to your local machine.
2. Be sure that `renv` is installed. If it's not, run the following code in the R console:

   ```
   install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))
   remotes::install_github('rstudio/renv')
   ```

3. Ensure that R's working directory is set to PINVAL's root directory, either using `setwd()` or using an [RStudio project](https://support.rstudio.com/hc/en-us/articles/200526207-Using-Projects) (suggested).
4. Install PINVAL's R library dependencies by running:

   ```
   renv::restore()
   ```
   This will install the specific packages and versions outlined in [renv.lock](./renv.lock) to a project-specific library under the `renv/` folder.

5. Activate the `renv` environment by running:

   ```
   renv::activate()
   ```
   This will ensure that R is using the project-specific libraries designated in [renv.lock](./renv.lock).

### .Renviron

The `.Renviron` [file](https://rstats.wtf/r-startup.html#renviron) is located on _each user's machine_. It loads [environmental variables](https://medium.com/chingu/an-introduction-to-environment-variables-and-how-to-use-them-f602f66d15fa) when R starts, and is typically used to store sensitive information such as API keys. The `.Renviron` file must be located in the user's R home directory to load properly. On Windows, this home directory is almost always `Documents`. In MacOS, the file is typically located at `~/.Renviron`.

PINVAL expects to find an environmental variable called `DB_CONFIG_CCAORSRV`. This variable is a database connection string formatted using the ODBC standard security syntax outlined [here](https://www.connectionstrings.com/microsoft-odbc-driver-17-for-sql-server/). For local development, this variable can be defined in the `.Renviron` file like so, changing variables (starting with `$`) to match your server settings.

```
DB_CONFIG_CCAORSRV=Driver={ODBC Driver 17 for SQL Server};Server=$SERVER_IP;Database=$DB_NAME;Uid=$DB_USER;Pwd=$DB_USER;
```

For MacOS users who installed the ODBC Driver [with Homebrew](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/install-microsoft-odbc-driver-sql-server-macos?view=sql-server-ver15), `Driver` should point directly to the ODBC `.dylib` installation file. The default path for this file is `/usr/local/lib/libmsodbcsql.17.dylib`.

To test if R loaded this environmental variable properly, run the following in the R console:

```
Sys.getenv("DB_CONFIG_CCAORSRV")
```

### Launching the Application

Once you've installed dependencies with `renv` and provided SQL credentials with the `.Renviron` file, you can launch the Shiny application to test any local changes you've made. To do so, simply open the `app.R` file in RStudio, then hit *Run App* in the top right of the text editing panel. Alternatively, you can use `shiny::runApp()` from the R console.

### Docker (Optional)

PINVAL uses [Docker](https://docs.docker.com/get-started/) and GitLab's integrated CI/CD for automatic builds and deployment. For the most part, anything that works on a local development version of PINVAL should also work when it is containerized. However, if you're familiar with Docker and would like to test a local build, complete the following:

1. Clone the repository and make any local changes, testing them using *Run App*.
2. If you've added additional libraries, be sure to run `renv::snapshot()` to update [renv.lock](./renv.lock).
3. Build the Docker container using the following command:

   ```
   docker build -t shiny_app_pinval:local .
   ```

4. Run the container using the following:

   ```
   docker run --env-file /path/to/.Renviron shiny_app_pinval:local

   ```
   Note that the .Renviron file must be loaded so that the Docker container can create the `DB_CONFIG_CCAORSRV` environmental variable.

5. Access the application by visiting `127.0.0.1:3838`.

## Deployment

PINVAL is deployed using GitLab's automated CI/CD pipelines. It's pipeline process is defined by [.gitlab-ci.yml](./.gitlab-ci.yml). The typical deployment process is:

1. A commit is made to the `staging` branch (the default), triggering a pipeline. Only commits to `staging` and `master`, or a new git tag, will trigger a build.
2. The pipeline defined in [.gitlab-ci.yml](./.gitlab-ci.yml) starts, the Docker image is built according to the repo's [Dockerfile](./Dockerfile).
3. The built Docker image is tagged with the hash that triggered the pipeline and pushed to the repository's container registry.
4. Depending on the branch, the built image is conditionally tagged:
   - If the branch is `staging`, the image is pulled from the registry and given an image tag *staging*. This branch/build is for testing new changes to the application.
   - If the branch is `master`, the image is pulled from the registry and given an image tag *latest*. This branch/build is intended to be a stable version of the application.
   - If a commit is tagged, the image is pulled from the registry and given the same tag as the commit. This allows for version-specific image builds for each major release.
5. Once the image is tagged, it is pushed back to the container registry. Another application, [shiny_server](https://gitlab.com/ccao-data-science---modeling/shiny_server) watches the container registry for changes to the *staging* and *latest* tags. When a change occurs, the server will download the new image and deploy it to any future users.

### Docker Caching

The [Dockerfile](./Dockerfile) in this repository makes use of Docker's caching. It is constructed to minimize the need to redownload/rerun layers of the build. For a brief primer on build caching, see the [Docker docs](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache).

GitLab's CI runners [do not natively allow use of Docker's caching](https://gitlab.com/gitlab-org/gitlab-foss/issues/17861), since each runner is a separate machine with no memory of previous builds.

To fix this problem and utilize Docker build caching, we push a new tagged image called `build-cache` to the registry after each commit. This image works to cache the previous build. As long as *no dependencies or steps have changed* in the [Dockerfile](./Dockerfile), the cache from the `build-cache` image is used and steps do not need to be re-run.

If any layers of the `build-cache` image are changed (for example, a new dependency is added to [renv.lock](./renv.lock)), then the build cache is busted and the image must be rebuilt from scratch.

## Code Structure

Due to the nature of its task, PINVAL's code structure is fairly linear. It takes two inputs (PIN, reporting year), queries a database, and outputs graphs, tables, and summary statistics. However, Shiny's reactive programming makes this simple task more complicated.

Plots and tables must be updated each time a new PIN is submitted. As such, any functions that are called when a PIN is submitted must be *reactive*, i.e. they must respond to events. In PINVAL's case, all ingest functions (from [R/ingest.R](./R/ingest.R)) are called on PIN submission. Each of the ingest functions has its own reactive (located in [R/reactives.R](./R/reactices.R).

These reactive wrappers are what get passed to plotting and table functions, which ultimately allows shiny to know that it must update these elements as new data is queried. Here is a sample flowchart for querying data for a plot on the Comps tab:

```mermaid
graph TD
	A[PIN Submitted] -->|PIN cleaned|B(mod_input.R)
	B --> |raw PIN value sent to ingest reactive|C{{get_diag_lst}}
  
  C --> |reactive expression <br/> triggers ingest function|H([ingest_diag_lst])
  H --> |ingest function <br/> runs SQL query|I(ingest_chars.sql)
  I --> |SQL queries CCAO DB|J[(CCAODATA)]

  D --> |reactive expression <br/> triggers ingest function|K([ingest_comps_df])
  K --> |ingest function <br/> runs SQL query|L(ingest_comps.sql)
  L --> |SQL queries CCAO DB|J[(CCAODATA)]
  
  C --> |PIN value passed to <br/> other ingest reactives|D{{get_comps_df}}
  C --> |reactive expression<br/>sent to module|E(mod_sales.R)
  D --> |reactive expression<br/>sent to module|E(mod_sales.R)
  E --> |on reactive update, i.e. PIN submission <br/> retrieve diag_lst and comps_df <br/> and pass to plot/table functions|F([plot_comps_sales function])
  F --> G((Plot displayed by Shiny))

  J --> |chars data <br/> sent to reactive|C
  J --> |comps data <br/> sent to reactive|D

```

### `app.R`

Main application file/launcher. Running this script will launch the app in the web browser. This file contains the `ui` object and `server` function that are needed to run a Shiny application. See [here](https://shiny.rstudio.com/articles/basics.html) for a basic overview of Shiny application structure.

### `test.R`

Simple script to test the functions defined in `R/` outside of the Shiny context. Useful for debugging and testing plots without needing to relaunch the app.

### `modules/`

PINVAL uses Shiny modules to help organize and discretize code. The modules are called within the `server` function of `app.R`, and their respective UI components are called within the `ui` object. For an overview of how modules work in Shiny, see [here](https://shiny.rstudio.com/articles/modules.html).

Modules in this directory are responsible for almost all components of the application. Each module script is prefixed with `mod_`. Modules for individual tabs, such as [mod_unif.R](./modules/mod_unif.R) and [mod_summ.R](./modules/mod_summ.R), contain the underlying server logic and UI structure for their respective tabs.

Other modules, such as [mod_gitlab.R](./modules/mod_gitlab.R) and [mod_download.R](./modules/mod_download.R), are responsible for creating individual elements of the app, such as the GitLab version number and data download buttons, respectively.

### `R/`

R files that contain functions used within `app.R` and `modules/`. Each function is prefixed with its broader category/functionality, e.g. an ingest function always starts with `ingest_`.

- `setup.R` loads libraries and other R scripts
- `checks.R` contains functions used with Shiny's [`validate()`](https://shiny.rstudio.com/reference/shiny/0.14/validate.html) function. Errors generated when the data is invalid are passed up to the UI element that calls the check.
- `helpers.R` contains helpers that complete a variety of small tasks.
- `ingest.R` contains wrappers for SQL queries. These functions first retrieve the data and then transform it into a format usable by PINVAL.
- `plots.R` contains all code for creating ggplot and plotly plots.
- `reactives.R` contains wrappers for ingest functions. These wrappers translate the static functions into reactive expressions that are called in response to an event. See [reference](https://shiny.rstudio.com/tutorial/written-tutorial/lesson6/).
- `tables.R` contains all code necessary to make static and dynamic tables.

### `SQL/`

SQL queries for data ingestion. The queries target CCAO's in-house MS SQL (T-SQL) database. PINVAL uses the [`glue_sql()` function](https://glue.tidyverse.org/reference/glue_sql.html) for string interpolation, meaning {var1}, {var2}, etc. within the query are replaced by their respective values from R.

See individual `.sql` files for comments on the purpose and structure of each query.

### `data/`

Data sources used by the application.

- `adjustments.rds` temporary dataset of adjustments made in 2020 in the wake of COVID-19.
- `p_types.csv` contains a human-readable list of type codes.
- `pin_list.csv` an arbitrary list of residential PINs used for testing. This list attempts to represent the full range of possible PIN configurations, including 288s, multi-codes, appeals, etc.

### `config/`

Configuration files used within the Docker container.

- `odbcinst.ini` defines the location of the ODBC driver used to connect to the CCAO database.
- `.Rprofile` is used to ensure that Shiny always launches on port 3838 so it can be picked up by `shiny_server`.

### `www/`

Files loaded by `app.R` automatically.

- `helpers.js` defines arbitrary javascript functions that can be called by [shinyjs](https://deanattali.com/shinyjs/).
- `jquery.js` defines jquery functions that add minor functionality tweaks.
- `style.css` is used to define the CSS styles of various objects in the app.

### `renv/` and `.Rprofile` and `renv.lock`

Files used by the `renv` package to maintain the project-specific package environment. See the [renv Infrastucture Section](https://rstudio.github.io/renv/articles/renv.html#infrastructure) for information on the function of each file.

### Docker/GitLab Related

Files used to build, test, and deploy the Dockerized version of PINVAL.

- `Dockerfile` defines the container dependencies and behavior.
- `.dockerignore` tells Docker which files to ignore when building an image.
- `.gitlab-ci.yml` defines the GitLab CI/CD pipeline steps. See [reference](https://docs.gitlab.com/ee/ci/yaml/)
- `.gitignore` tells git to ignore certain files that don't need to be committed.
- `.gitlab/issue_templates` holds templates for GitLab issues in the project. See [reference](https://docs.gitlab.com/ee/user/project/description_templates.html)

## Built With

* [Shiny](https://shiny.rstudio.com/) - Web framework
* [renv](https://rstudio.github.io/renv/articles/renv.html) - Dependency management
* [rocker](https://hub.docker.com/r/rocker/shiny) - Base image

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on the process for submitting merge requests.

## Maintainers

* **Dan Snow** - *Initial development* - [dfsnow](https://gitlab.com/dfsnow)

## License

This project is licensed under the GNU AGPLv3 License - see the [LICENSE.md](./LICENSE.md) file for details
