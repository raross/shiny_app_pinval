// Mini function that hides the print tab on application start
// This tab can then only be accessed by using the Show Full Report button
shinyjs.init = function() {
  $("#pin_input-PIN").select();
  $('#tabs li a[data-value="print_tab"]').hide();
};

// Mini function that calls the print dialog on click
shinyjs.winprint = function() {
  window.print();
};