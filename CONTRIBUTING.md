# Contributing

### Contributors External to CCAO

The Cook County Assessor's Office has a Developer Engagement Program for external contributors. By partnering with developers, coders, data scientists, and others, the CCAO hopes to integrate external knowledge and allow outside contributors to build solutions that advance the mission of the agency.

Please review the documents in [collaborator.documents](https://gitlab.com/ccao-data-science---modeling/ccao_sf_cama_dev/tree/master/collaborator.documents) for additional information.

### Contributors Internal to CCAO

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with a CCAO Senior Data Scientist.

## Setup Process

This application uses renv for dependency management and version locking.
Please read the [renv documentation](https://rstudio.github.io/renv/articles/renv.html) before proceeding.

A general contribution workflow would likely be:

1. Clone the repository using git.
2. Create your project environment using renv by running `renv::restore()` in the same directory
   as the `renv.lock` file. This will download all the packages needed for this project.
3. Ensure that you've activated the renv project environment, either by opening the repository as an
   R project, or by using `renv::activate()` while the project folder is your working directory.
4. Make the changes you need to the code.
5. Test your changes by launching the application locally. You can do this by hitting the
   *Run App* button that appears when `app.R` is open. Or by using `shiny::runApp(...)`.
6. Open a merge request on GitLab, commit your changes, then request approval.

## Merge Request Process

When making a merge request, please ensure that you complete the following steps:

1. Ensure that you've updated the `renv.lock` file with any additional packages you've
   decided to use. This can be accomplished by simply running `renv::snapshot()` after
   adding libraries to your code.
2. Remove any intermediate files created by R, such as HTML reports, .md files, or .tex files.
3. Run both [styler](https://styler.r-lib.org/) and [lintr](https://github.com/jimhester/lintr)
   on the code you changed. These libraries will ensure that your code is clean and free of errors.
4. Ensure that no functions are making implicit calls to objects. Be sure that everything
   used inside a function is _passed to that function_. To check that this is the case,
   you can restart R to clear the environment and then re-test the application.
