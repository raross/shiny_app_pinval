/* 

Gather basic statistics on appeals for CCAO appeals only. Board of Review 
appeals are joined onto this data in ingest_appeals_df()

*/

SELECT 
  APP.PIN,
  APP.TAX_YEAR,
  CONCAT(PC_DESC_CD3A, PC_DESC_CD3B) AS DESCRIPTION,
  CASE WHEN PC_PIN_RESULT_3 != '' AND PC_PIN_RESULT_2 = '' THEN 1
    WHEN PC_PIN_RESULT_2 != '' AND PC_PIN_RESULT_1 = '' THEN 2
    WHEN PC_PIN_RESULT_1 != '' THEN 3
    ELSE 0 
    END AS [NUM REVIEWED],
  PC_T_TOTAL AS MAILED,
  PC_TB_TOTAL AS CERTIFIED
FROM dbo.APPEALSDATA APP
WHERE APP.PIN = {val1}
AND APP.TAX_YEAR >= {val2}