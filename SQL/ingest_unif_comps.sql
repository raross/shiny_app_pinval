/* The goal of this query is to replicate the uniformity comparable finding
algorithm of CCAO's AS400. The algorithm is quite simple and works by simply
buffering the parameters of the target PIN by a fixed percentage. By default,
the algorithm matches using the following settings:

Target PIN   --   Comp PIN
Neighborhood =    Neighborhood
Class        =    Class
Ext. Wall    =    Ext. Wall
Bldg. SF     =    Bldg. SF +/- 15%
Land SF      =    Land SF +/- 15%
Age          =    Age +/- 20%
$/bldg SF    =    $/bldg SF +/- 50%

*/

-- Declare variables defining the subject PIN's characteristics that can be 
-- used in a second query
DECLARE @Pin varchar(14)
DECLARE @Neighborhood int
DECLARE @Classification int
DECLARE @Town int
 
DECLARE @SubjectPINBuildingSquareFeet float
DECLARE @SubjectPINLandSquareFeet float
DECLARE @SubjectPINAge float
DECLARE @SubjectPINExt float
DECLARE @SubjectPINBuildingAV float

-- Query to find the subject PIN's characteristics and set the declared vars
SELECT 
  @Pin = T.PIN,
  @SubjectPINBuildingSquareFeet = CHARS.BLDG_SF, 
  @SubjectPINLandSquareFeet = CHARS.HD_SF,
  @SubjectPINAge = CHARS.AGE,
  @SubjectPINExt = CHARS.EXT_WALL,
  @SubjectPINBuildingAV = T.HD_ASS_BLD,
  @Neighborhood = T.HD_NBHD,
  @Classification = T.HD_CLASS,
  @Town = LEFT(T.HD_TOWN, 2)
FROM AS_HEADT T
INNER JOIN CCAOSFCHARS AS CHARS
  ON T.PIN = CHARS.PIN AND T.TAX_YEAR = CHARS.TAX_YEAR
WHERE T.PIN = {val1}
AND T.TAX_YEAR = {val2}


-- Query to find uniformity comparables based on the subject PIN and parameters
SELECT DISTINCT TOP (100) T.PIN,
  CASE WHEN T.PIN = @Pin THEN 1 ELSE 0 END AS TARG_PIN,
  T.HD_CLASS AS CLASS,
  T.TAX_YEAR,
  LEFT(T.HD_TOWN, 2) AS TOWN,
  T.HD_NBHD AS NBHD,
  CHARS.BLDG_SF AS BLDG_SF,
  CHARS.HD_SF AS LND_SF,
  CHARS.AGE,
  CHARS.FBATH,
  CHARS.HBATH,
  CHARS.AIR,
  CHARS.SITE,
  CHARS.ROOF_CNST,
  CHARS.GAR1_SIZE,
  CHARS.FRPL,
  CHARS.BSMT,
  CHARS.BSMT_FIN,
  CHARS.PORCH,
  CHARS.EXT_WALL,
  CHARS.HEAT,
  (T.HD_ASS_BLD + T.HD_ASS_LND) AS ASSMNT_VAL,
  (T.HD_ASS_BLD / CHARS.BLDG_SF) * 10 AS DOLLAR_SQFT,
  PL.centroid_x,
  PL.centroid_y
FROM AS_HEADT T
INNER JOIN (
	SELECT PIN, MAX(TAX_YEAR) as most_recent_year
	FROM AS_HEADT
	GROUP BY PIN
) A ON A.PIN = T.PIN AND A.most_recent_year = T.TAX_YEAR
INNER JOIN CCAOSFCHARS AS CHARS
  ON CHARS.PIN = T.PIN AND CHARS.TAX_YEAR = {val2}
LEFT JOIN DTBL_PINLOCATIONS AS PL
  ON T.PIN = PL.PIN
WHERE T.HD_NBHD = @Neighborhood
AND T.HD_CLASS = @Classification
AND LEFT(T.HD_TOWN, 2) = @Town
AND CHARS.BLDG_SF 
  BETWEEN (@SubjectPINBuildingSquareFeet - (@SubjectPINBuildingSquareFeet * 0.15)) 
  AND (@SubjectPINBuildingSquareFeet + (@SubjectPINBuildingSquareFeet * 0.15))
AND CHARS.HD_SF 
  BETWEEN (@SubjectPINLandSquareFeet - (@SubjectPINLandSquareFeet * 0.15)) 
  AND (@SubjectPINLandSquareFeet + (@SubjectPINLandSquareFeet * 0.15))
AND CHARS.AGE 
  BETWEEN (@SubjectPINAge - (@SubjectPINAge * 0.2)) 
  AND (@SubjectPINAge + (@SubjectPINAge * 0.2))
AND T.HD_ASS_BLD / CHARS.BLDG_SF
  BETWEEN ((@SubjectPINBuildingAV / @SubjectPINBuildingSquareFeet) -
          ((@SubjectPINBuildingAV / @SubjectPINBuildingSquareFeet) * 0.5)) 
	AND ((@SubjectPINBuildingAV / @SubjectPINBuildingSquareFeet) +
	    ((@SubjectPINBuildingAV / @SubjectPINBuildingSquareFeet) * 0.5))
AND CHARS.EXT_WALL = @SubjectPINExt
ORDER BY TARG_PIN DESC, T.PIN

